import numpy as np
import cv2
import os
import math
import geopy.distance
from operator import itemgetter, attrgetter

def area(x1, y1, x2, y2, x3, y3): 
    return abs((x1 * (y2 - y3) 
                + x2 * (y3 - y1)  
                + x3 * (y1 - y2)) / 2.0)


def isInside(x1, y1, x2, y2, x3, y3, x, y): 
    A = area (x1, y1, x2, y2, x3, y3)  # Calculate area of triangle ABC 
    A1 = area (x, y, x2, y2, x3, y3)  # Calculate area of triangle PBC  
    A2 = area (x1, y1, x, y, x3, y3) # Calculate area of triangle PAC  
    A3 = area (x1, y1, x2, y2, x, y) # Calculate area of triangle PAB  
    TotalArea = A1 + A2 + A3
    return (A - TotalArea < 5 and A - TotalArea > -5 )  # Check if sum of A1, A2 and A3  is same as A with tolerance of 5 pixel square


def check_if_in_detection_zone(detection_zone, position):
    if detection_zone is None:
        return True
    if( detection_zone[0][0] < position[0] < detection_zone[1][0] or detection_zone[0][0] > position[0] > detection_zone[1][0]):
        if(detection_zone[0][1] < position[1] < detection_zone[1][1] or detection_zone[0][1] > position[1] > detection_zone[1][1]):
            return True
    return False


def return_center(box_input):
    width = box_input[2] - box_input[0]
    height = box_input[3] - box_input[1]
    x_center = box_input[0] + 0.5 * width
    y_center = box_input[1] + 0.5 * height
    return int(x_center), int(y_center)


def check_lane(coordinate , lanes):
    lane = -1
    for i in range(len(lanes)-1):
        if (Check_between_two_polyline (lanes[i], lanes[i+1], coordinate)):
            lane = i
            return lane
    return lane


def Check_between_two_polyline(lane_1, lane_2 , coordinate):
    inside = False;
    for i in range (len(lane_1)-1):
        for j in range(len(lane_2)):
            inside = isInside(lane_1[i][0], lane_1[i][1], lane_1[i+1][0], lane_1[i+1][1], lane_2[j][0],lane_2[j][1], coordinate[0], coordinate[1])
            if(inside):
                return True

    for i in range (len(lane_2)-1):
        for j in range(len(lane_1)):
            inside = isInside(lane_2[i][0], lane_2[i][1], lane_2[i+1][0], lane_2[i+1][1], lane_1[j][0],lane_1[j][1], coordinate[0], coordinate[1])
            if(inside):
               return True
    return False


def return_lane_config(lane_value):
    lane = lane_value.split("|")
    return lane


def getLaneConfig(lane_config):
    x,y = [],[]
    lanes = [] #this will be a 3 dimensional array, first value is either 0 or one for x and y, second one is for lane, while third one is the point in the lane
    for i in range(1000):
        line=[]
        try:
            lane_data = return_lane_config(lane_config["line" + str(i+1)])
            for j in range (1000):
                try:
                    point =[]
                    for k in range(2):
                        point.append(int(lane_data[j].split(", ")[k]))
                        if (k == 0):
                            x.append(int(lane_data[j].split(", ")[k]))
                        else:
                            y.append(int(lane_data[j].split(", ")[k]))
                except:
                    break
                line.append(point)
        except:
            break
        lanes.append(line)
    detection_zone = [[min(x),min(y)],[max(x),max(y)]]
    video_path = lane_config["video_path"]
    return  np.array(lanes, dtype = object), np.array(detection_zone, dtype = object), video_path


quad_coords = {
    "lonlat": np.array([
        [3.098096434575572, 101.67597099750341], # Third lampost top right
        [3.0980946969602012, 101.67607565821791], # Corner of white rumble strip top left
        [3.09777503033728, 101.67591467110569], # Corner of rectangular road marking bottom left
        [3.0978133288212297, 101.67607677265491] # Corner of dashed line bottom right
    ]),
    "pixel": np.array([
        [195, 216], # Third lampost top right
        [394, 197 ], # Corner of white rumble strip top left
        [299, 526], # Corner of rectangular road marking bottom left
        [595, 350] # Corner of dashed line bottom right
    ]),
     "lonlat_901_lotLat": np.array([
        [3.0907259853103093, 101.68074719440041], #lampost on the triangle at left sides
        [3.0906350604653245, 101.67999287644918], # Corner of white rumble strip top left
        [3.0908172162625513, 101.6802484868174], # Lampost on right side
        [3.0907671409599353, 101.68076105923666] # Traffic light
    ]),
    "pixel_901_1000": np.array([
        [454, 660], # lampost on the triangle at left side
        [698, 365 ], # Fourth Lampost from right
        [855, 384], # Lampost on right side
        [843, 635] # Traffic light
    ]),
}


class PixelMapper(object):
    """
    Create an object for converting pixels to geographic coordinates,
    using four points with known locations which form a quadrilteral in both planes
    Parameters
    ----------
    pixel_array : (4,2) shape numpy array
        The (x,y) pixel coordinates corresponding to the top left, top right, bottom right, bottom left
        pixels of the known region
    lonlat_array : (4,2) shape numpy array
        The (lon, lat) coordinates corresponding to the top left, top right, bottom right, bottom left
        pixels of the known region
    """
    def __init__(self, pixel_array, lonlat_array):
        assert pixel_array.shape==(4,2), "Need (4,2) input array"
        assert lonlat_array.shape==(4,2), "Need (4,2) input array"
        self.M = cv2.getPerspectiveTransform(np.float32(pixel_array),np.float32(lonlat_array))
        self.invM = cv2.getPerspectiveTransform(np.float32(lonlat_array),np.float32(pixel_array))
        
    def pixel_to_lonlat(self, pixel):
        """
        Convert a set of pixel coordinates to lon-lat coordinates
        Parameters
        ----------
        pixel : (N,2) numpy array or (x,y) tuple
            The (x,y) pixel coordinates to be converted
        Returns
        -------
        (N,2) numpy array
            The corresponding (lon, lat) coordinates
        """
        if type(pixel) != np.ndarray:
            pixel = np.array(pixel).reshape(1,2)
        assert pixel.shape[1]==2, "Need (N,2) input array" 
        pixel = np.concatenate([pixel, np.ones((pixel.shape[0],1))], axis=1)
        lonlat = np.dot(self.M,pixel.T)
        
        return (lonlat[:2,:]/lonlat[2,:]).T[0]

    def lonlat_to_pixel(self, lonlat):
        """
        Convert a set of lon-lat coordinates to pixel coordinates
        Parameters
        ----------
        lonlat : (N,2) numpy array or (x,y) tuple
            The (lon,lat) coordinates to be converted
        Returns
        -------
        (N,2) numpy array
            The corresponding (x, y) pixel coordinates
        """
        if type(lonlat) != np.ndarray:
            lonlat = np.array(lonlat).reshape(1,2)
        assert lonlat.shape[1]==2, "Need (N,2) input array" 
        lonlat = np.concatenate([lonlat, np.ones((lonlat.shape[0],1))], axis=1)
        pixel = np.dot(self.invM,lonlat.T)
        return (pixel[:2,:]/pixel[2,:]).T


def findDistance(GPS1, GPS2):
      return geopy.distance.distance(GPS1,GPS2).m


def checkDistancefromPixel(pixel_1,pixel_2):
    #convert pixel to coordinate & calculate the difference(findDistance)
      pm = PixelMapper(quad_coords["pixel"], quad_coords["lonlat"])
      coordinate_1 = pm.pixel_to_lonlat(pixel_1)
      coordinate_2 = pm.pixel_to_lonlat(pixel_2)
      return findDistance(coordinate_1,coordinate_2)
  

def clear():
    os.system('cls' if os.name=='nt' else 'clear')


def draw_lane(frame, lanes, thickness = 1, color=(255,255,255)):
    for lane in lanes:
        for i in (range(len(lane)-1)):
            cv2.line(frame, (lane[i][0], lane[i][1]), (lane[i+1][0], lane[i+1][1]), color, thickness)


def InitializeVariables(lane_number):
    lane_total_car =np.zeros(lane_number)
    lane_total_speed =np.zeros(lane_number)
    lane_queue_length =np.zeros(lane_number)
    lane_queue = []
    for i in range(lane_number):
        lane_queue.append([])
    return lane_total_car, lane_queue_length, lane_total_speed, lane_queue


def measure_and_draw_queue_line(frame, lane_queue, car_lane_queue_length, threshold_limit = 100): #use this definition for object_tracker.py

    for j in range(len(lane_queue)):
        lane = lane_queue[j]
        try:
            lane.sort(key = lambda x:x[1])
            #sorted(lane, key=itemgetter(1))
        except :
            continue
        for i in (range(len(lane)-1)):
            if(checkDistancefromPixel(lane[i],lane[i+1]) < threshold_limit):
                cv2.line(frame, (lane[i][0], lane[i][1]), (lane[i+1][0], lane[i+1][1]), (255,255,255), 3) #lane1
                try:
                    car_lane_queue_length[j] += checkDistancefromPixel(lane[i],lane[i+1])
                except:
                    car_lane_queue_length[j] += 0
            else:
                break

def measure_draw_queue(frame,ref_point_lane, sorted_car, speed = 0,  threshold_limit = 100 , line2car=10, red2green = 0): #use this definition for vehicle_tracker.py

        queue_length = 0
        distance = checkDistancefromPixel((sorted_car[0].carCurrentPosition),ref_point_lane)
        #print("Distance : " + str(distance ))
        if(red2green == 0):
            if(distance > line2car or speed > 0):   #line2car distance and speed s fail safe system to avoid fake queue line
            #for troubleshoot (draw black line)
                #cv2.line(frame, (sorted_car[0].carCurrentPosition), ref_point_lane, (0,0,0), 3) #lane1
                return 0
        # for troubleshoot (draw white line when reach the line to 1st car distance)
        #cv2.line(frame, (sorted_car[0].carCurrentPosition), ref_point_lane, (255,255,255), 3) #lane1

        # loop for every car eligible for queue length and accumulate the queue length and update
        for i in (range(len(sorted_car)-1)):
            distance_between_2car = checkDistancefromPixel(sorted_car[i].carCurrentPosition,sorted_car[i+1].carCurrentPosition)
            #cv2.putText(frame, str(i) ,tuple(sorted_car[i].carCurrentPosition),0, 2, (255,255,255),4)
            # i = Which car, compare 1st car with next car, threshold distance (can change limit = car2car) = 100, if more, no queue length)
            if(distance_between_2car < threshold_limit):
                cv2.line(frame, (sorted_car[i].carCurrentPosition), (sorted_car[i+1].carCurrentPosition), set_color(speed), 3) #lane1
                # if 1st and 2nd car fulfilled, update queue length value
                try:
                    queue_length += distance_between_2car
                    continue
                except:
                    queue_length += 0
            else:
                break
            if(red2green == 0):
                break
        return queue_length


def set_color(speed):
    Red =(255,0,0)
    Yellow = (255,255,0)
    Green = (0,255,0)
    if speed < 5:
        return Red
    if speed > 20:
        return Green
    return Yellow 

def Calculate_counting_speed_queue(track, number_of_lane, lane_queue, car_lane_number, car_lane_speed):
    for i in range(number_of_lane):
        if(track.lane == i):
            car_lane_number[i] += 1 
            car_lane_speed[i] += track.speed
            lane_queue[i].append(track)
            break
    return i

def createPixelMaper(junction = ''):
    if junction == '901':
        pm = PixelMapper(quad_coords["pixel_901_1000"], quad_coords["lonlat_901_lotLat"])
    else:
        pm = PixelMapper(quad_coords["pixel"], quad_coords["lonlat"])
    return pm

def maintain_aspect_ratio_resize(frame, width=None, inter=cv2.INTER_AREA):
        # Grab the image size and initialize dimensions
        (h, w) = frame.shape[:2]
        dimension = (width, int(h * width / float(w)))
        frame = cv2.resize(frame, dimension, interpolation=cv2.INTER_AREA)
        #image = Image.fromarray(frame)
        return frame    