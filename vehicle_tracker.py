import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import time
import tensorflow as tf
physical_devices = tf.config.experimental.list_physical_devices('GPU')
if len(physical_devices) > 0:
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
from absl import app, flags
from absl.flags import FLAGS
import core.utils as utils
from core.yolov4 import filter_boxes
from tensorflow.python.saved_model import tag_constants
from query import *
from core.config import cfg
from PIL import Image
import cv2
import numpy as np
import matplotlib.pyplot as plt
#import websockets
from websocket import create_connection
from websocketJc3 import SendWebSocket
from jc3Api import SendDataToApi
import websocket
import asyncio
from threading import Thread
import threading
from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
# deep sort imports
from deep_sort import preprocessing, nn_matching
from deep_sort.detection import Detection
from deep_sort.tracker import Tracker
import generate_detections as gdet
from checkinbox import *
from config import lane_config, database_config
from pgresql import *
flags.DEFINE_string('framework', 'tf', '(tf, tflite, trt')
flags.DEFINE_string('weights', './checkpoints/yolov4-416', 'path to weights file')
flags.DEFINE_integer('size', 416, 'resize images to')
flags.DEFINE_boolean('tiny',True, 'yolo or yolo-tiny')
flags.DEFINE_string('model', 'yolov4', 'yolov3 or yolov4')
flags.DEFINE_list('junction', ['903'], 'Junction code')
flags.DEFINE_string('video', 'data/video/test.mp4', 'path to input video or set to 0 for webcam')
flags.DEFINE_string('output', None, 'path to output video')
flags.DEFINE_string('output_format', 'XVID', 'codec used in VideoWriter when saving video to file')
flags.DEFINE_float('iou', 0.5, 'iou threshold')
flags.DEFINE_float('score', 0.50, 'score threshold')
flags.DEFINE_boolean('dont_show', False, 'dont show video output')
flags.DEFINE_boolean('info', False, 'show detailed info of tracked objects')
flags.DEFINE_boolean('count', True, 'count objects being tracked on screen')
flags.DEFINE_boolean('SendToDatabase', False, 'Send to Database')
flags.DEFINE_boolean('SendToWebSocket', False, 'Send to Web Socket')
flags.DEFINE_boolean('SendToApi', True, 'Send to Api')
flags.DEFINE_integer('car2car', 25, 'Car to car distance trigger threshold')
flags.DEFINE_integer('line2car',20,'Distance between 1st reference lane point to 1st car for Zone System')
flags.DEFINE_integer('width', 300, 'Width of the windows')
flags.DEFINE_string('database', 'database_huawei', 'Database to send data')
flags.DEFINE_string('WebSocketIP', 'ws://127.0.0.1:5001/EchoAll', 'WebSocket Ip')
from threading import Thread


def main(_argv):
    ws = None
    if(FLAGS.SendToWebSocket):
        websocket.enableTrace(False)
        try:
            ws = create_connection("ws://127.0.0.1:5001/EchoAll")
        except :
            FLAGS.SendToWebSocket = False
            print('Warning, Websocket Connection could not be made, disabling websocket now..')
        

    # Definition of the parameters
    nms_max_overlap = 1.0
    
    # initialize deep sort
    model_filename = 'model_data/mars-small128.pb'
    encoder = gdet.create_box_encoder(model_filename, batch_size=1)
   
   

    # load configuration for object detector
    config = ConfigProto()
    config.gpu_options.allow_growth = True
    session = InteractiveSession(config=config)
    STRIDES, ANCHORS, NUM_CLASS, XYSCALE = utils.load_config(FLAGS)
    #video_path = FLAGS.video
    saved_model_loaded = tf.saved_model.load(FLAGS.weights, tags=[tag_constants.SERVING])
    infer = saved_model_loaded.signatures['serving_default']


    junctions, threads = [],[]
    for junction in FLAGS.junction:
        junctions.append(VideoStreamWidget(ws,junction,infer, encoder))

    for junction in junctions:
        threads.append(Thread(target=do_inference, args=(junction,)))

    for thread in threads:
        thread.daemon = True
        thread.start()

    while True:
        time.sleep(0.1)
        if FLAGS.dont_show:
           pass
        else:
            try:
                im_v = junctions[0].result
                for i in range(1 , len(junctions)):
                    im_v = cv2.vconcat([im_v,junctions[i].result])
                    
                #if FLAGS.stream:
                cv2.imshow(f"Junction Combine",im_v)
                key = cv2.waitKey(1)
                if key == ord('q'):
                    cv2.destroyAllWindows()
                    exit(1)
            except Exception as e:
                pass

def do_inference(video_stream_widget):
    while True:
         time.sleep(0.01)
         try:
            video_stream_widget.show_frame()
         except AttributeError:
            pass

class VideoStreamWidget(object):
    def __init__(self, ws, junction,infer, encoder): 
        self.encoder = encoder
        self.infer = infer
         # calculate cosine distance metric
        max_cosine_distance = 0.4
        nn_budget = None
        metric = nn_matching.NearestNeighborDistanceMetric("cosine", max_cosine_distance, nn_budget)
        self.tracker = Tracker(metric)
         # initialize tracker
        self.frame_num = 0
        self.ws = ws
        self.fps = 1
        self.junction = junction
        src = getLaneConfig(lane_config(self.junction))[2]

        #lanes, detection_zone, video_path get information from database.ini
        self.lanes, self.detection_zone, self.video_path = getLaneConfig(lane_config(self.junction))
        self.red2green= np.zeros(len(self.lanes))
        self.pm = createPixelMaper(junction)
        self.status = True
        if(FLAGS.SendToDatabase):
            self.TimeDatabase = time.time()
            self.Connectionpool = CreatePool(database_config(section=FLAGS.database))
            Insert_lane_if_not_exist(junction, len(self.lanes)-1, self.Connectionpool)
        self.SendToDatabaseOpen = FLAGS.SendToDatabase
        

        if(FLAGS.SendToWebSocket):
            self.TimeWebSockets = time.time()
        self.WebSocketOpen = FLAGS.SendToWebSocket

        if(FLAGS.SendToApi):
            self.TimeApi = time.time()
        self.SendToApi = FLAGS.SendToApi
     

        # Create a VideoCapture object
        self.capture = cv2.VideoCapture(src)

        # Start the thread to read frames from the video stream
        self.thread = Thread(target=self.update, args=())
        self.thread.daemon = True
        self.thread.start()

    def update(self):
        # Read the next frame from the stream in a different thread
        while True:

            if self.capture.isOpened():
                (self.status, self.frame) = self.capture.read()
                self.frame_num +=1

                #input_size = FLAGS.size
                ##image_data will be copied from frame, and it will be resized into 416, and it will be fed into YOLOv4
                #if(frame is not None):
                #    self.frame = cv2.resize(frame, (input_size, input_size))

            else:
                self.capture = cv2.VideoCapture(self.src)

    def show_frame(self):
        sendToDatabase = False
        # show_frame handles send data to database, change box colour, show video, tracking, queue length
        SendToWebSocket = False
        SendToApi = False
        if(self.SendToDatabaseOpen and time.time()-self.TimeDatabase > 0.7):
            sendToDatabase = True
            self.TimeDatabase = time.time()
        else:
            sendToDatabase = False

        if(self.WebSocketOpen and time.time() - self.TimeWebSockets > 0.5):
            SendToWebSocket = True
            self.TimeWebSockets = time.time()
        else:
            SendToWebSocket = False

        if(self.SendToApi and time.time() - self.TimeApi > 1):
            SendToApi = True
            self.TimeApi = time.time()
        else:
            SendToApi = False

        
        tracker = self.tracker
        frame = self.frame
        lanes = self.lanes
        pm = self.pm
        fps=self.fps

        (original_height, original_width) = frame.shape[:2]
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        new_width = 1000

        input_size = FLAGS.size
        #print('Frame #: ', self.frame_num)
        frame_size = frame.shape[:2]
        #image_data will be copied from frame, and it will be resized into 416, and it will be fed into YOLOv4
        image_data = cv2.resize(frame, (input_size, input_size))
        ratio = float(input_size/new_width)
        cv2.rectangle(image_data, (0,0), ( int(self.detection_zone[0][0]*ratio), int(original_height)), (0,0,0),-1)

        image_data = image_data / 255.
        image_data = image_data[np.newaxis, ...].astype(np.float32)     
        start_time = time.time()

        frame = maintain_aspect_ratio_resize(frame, width=new_width)
        
        batch_data = tf.constant(image_data)
        pred_carBox = self.infer(batch_data)
        for key, value in pred_carBox.items():
                boxes = value[:, :, 0:4]
                pred_conf = value[:, :, 4:]

        boxes, scores, classes, valid_detections = tf.image.combined_non_max_suppression(
            boxes=tf.reshape(boxes, (tf.shape(boxes)[0], -1, 1, 4)),
            scores=tf.reshape(
                pred_conf, (tf.shape(pred_conf)[0], -1, tf.shape(pred_conf)[-1])),
            max_output_size_per_class=50,
            max_total_size=50,
            iou_threshold=FLAGS.iou,
            score_threshold=FLAGS.score
        )

        # convert data to numpy arrays and slice out unused elements
        num_objects = valid_detections.numpy()[0]
        carBoxes = boxes.numpy()[0]
        carBoxes = carBoxes[0:int(num_objects)]
        scores = scores.numpy()[0]
        scores = scores[0:int(num_objects)]
        classes = classes.numpy()[0]
        classes = classes[0:int(num_objects)]

        # format bounding boxes from normalized ymin, xmin, ymax, xmax ---> xmin, ymin, width, height
        original_h, original_w, _ = frame.shape
        carBoxes = utils.format_boxes(carBoxes, original_h, original_w)

        # store all predictions in one parameter for simplicity when calling functions
        pred_carBox = [carBoxes, scores, classes, num_objects]

        # read in all class names from config
        class_names = utils.read_class_names(cfg.YOLO.CLASSES)

        # by default allow all classes in .names file
        #allowed_classes = list(class_names.values())
        
        # custom allowed classes (uncomment line below to customize tracker for only people)
        allowed_classes = ['car', 'motorbike','bus','truck','person']

        # loop through objects and use class index to get class name, allow only classes in allowed_classes list
        names = []
        deleted_indx = []
        for i in range(num_objects):
            class_indx = int(classes[i])
            class_name = class_names[class_indx]
            if class_name not in allowed_classes:
                deleted_indx.append(i)
            else:
                names.append(class_name)
        names = np.array(names)
        count = len(names)
       
        # delete detections that are not in allowed_classes
        carBoxes = np.delete(carBoxes, deleted_indx, axis=0)
        scores = np.delete(scores, deleted_indx, axis=0)

        # encode yolo detections and feed to tracker
        features = self.encoder(frame, carBoxes)
        detections = [Detection(carBox, score, class_name, feature) for carBox, score, class_name, feature in zip(carBoxes, scores, names, features)]

        #initialize color map
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i)[:3] for i in np.linspace(0, 1, 20)]

        # run non-maxima supression
        boxs = np.array([d.tlwh for d in detections])
        scores = np.array([d.confidence for d in detections])
        classes = np.array([d.class_name for d in detections])
        indices = preprocessing.non_max_suppression(boxs, classes, 1.0, scores)
        detections = [detections[i] for i in indices]       
        lane_total_car, lane_queue_length, lane_total_speed, lane_queue = InitializeVariables(len(lanes)-1)

        # Call the tracker
        tracker.predict()
        tracker.update(detections)
        # update tracks
        # to track each object

        k = 0
        for track in tracker.tracks:
            try:
                track.coordinate 
                init = False
            except:
                track.speed,track.lane = 0,-1
                init = True
                track.carCurrentPosition = None
            if not track.is_confirmed() or track.time_since_update > 1:
                continue 
            carBox = track.to_tlbr()

            if(check_if_in_detection_zone(self.detection_zone, carBox)):
                track.carCurrentPosition = return_center(carBox)

                track.lane = check_lane(track.carCurrentPosition, lanes)
                #if(track.lane == 3):
                #    starttime = time.time()
                #    for i in range (5000):
                #        track.lane = check_lane(track.carCurrentPosition, lanes)
                #    print(str((time.time()-starttime)/5000))
               
                if(track.lane == -1):
                    continue
                new_coordinate = pm.pixel_to_lonlat(track.carCurrentPosition)
                if not (init):
                    distance = findDistance(new_coordinate,track.coordinate)
                    track.speed = distance*fps/5*18
                    Calculate_counting_speed_queue(track, len(lanes)-1, lane_queue, lane_total_car, lane_total_speed)
                track.coordinate = new_coordinate


                color = colors[int(track.track_id) % len(colors)]
                color = [i * 255 for i in color]
                               
                #draw carBox on screen
                cv2.rectangle(frame, (int(carBox[0]), int(carBox[1])), (int(carBox[2]), int(carBox[3])), color, 1)
                cv2.rectangle(frame, (int(carBox[0]), int(carBox[1]-25)), (int(carBox[0] +carBox[2] - carBox[0]), int(carBox[1])), color, -1)
                cv2.putText(frame,  str('{0:.0f}'.format(track.speed)) + " kmh",(int(carBox[0]), int(carBox[1]-3)),1, 0.6, (255,255,255),1)
                cv2.putText(frame, "L" + str(track.lane + 1) ,(int(carBox[0]), int(carBox[1]-13)),0, 0.3, (255,255,255),1)
                                
                 # if enable info flag then print details about each track
                if FLAGS.info:
                    print("Tracker ID: {}, Class: {},  carBox Coords (xmin, ymin, xmax, ymax): {}".format(str(track.track_id), class_name, (int(carBox[0]), int(carBox[1]), int(carBox[2]), int(carBox[3]))))

        fps = 1.0 / (time.time() - start_time)
        print("FPS: %.2f" % fps)
        lane_average_speed = np.zeros(len(lane_total_speed))

        #loop for each lane to monitor average speed & queue length
        for i in range(len(self.lanes)-1):  #range(len(x)-1) (number of line minus 1) automatic to know how many lanes
            if(lane_total_car[i] != 0):
                # establish average speed (i=lane number)
                lane_average_speed[i] = lane_total_speed[i]/lane_total_car[i]
                # establish queue length (i=lane number)
                FrontTrack = None
                try:
                    lane_queue[i] = sorted(lane_queue[i], key=lambda x: x.carCurrentPosition[1], reverse=True)
                except Exception as e:
                    print(e)
                FrontTrack = lane_queue[i][0]
                
                if(FrontTrack != None):
                    frontSpeed = FrontTrack.speed                                
                    cv2.circle(frame, FrontTrack.carCurrentPosition, 10, (255,255,255), 3)  #indicator for 1st car detection

                    length =  measure_draw_queue(frame,tuple(self.lanes[i][0]),lane_queue[i], frontSpeed,FLAGS.car2car, FLAGS.line2car,self.red2green[i])
                    if(length != 0):
                        self.red2green[i] = 1
                    else:
                        self.red2green[i] = 0
                    lane_queue_length[i] = length
                
            else:
                lane_average_speed[i] = -1

            #print("Lane " + str(i+1)," :", lane_total_car[i], " cars, " , str('{0:.0f}'.format(lane_average_speed[i])) , " km/h ", " Queue length : ", str('{0:.0f}'.format(lane_queue_length[i])) + " m")
            #if FLAGS.count:
                #cv2.putText(frame, f"L{i + 1}: {lane_total_car[i]}, Speed: {str('{0:.0f}'.format(lane_average_speed[i]))}, QLength: " + str('{0:.0f}'.format(lane_queue_length[i])) + " m", (5, 30*(i+2)), 0 , 0.5, (255, 255, 255),1)
            #cv2.putText(frame, f"FPS : {str('{0:.2f}'.format(fps))}", (5, 30*(i+2) + 30), 0 , 0.5, (255, 255, 255),1)
        
        if sendToDatabase:
            x = threading.Thread(target = Update_database, args=(self.junction, lane_average_speed, lane_queue_length, lane_total_car , self.Connectionpool))
            x.start()
        if SendToWebSocket:
            try:
                SendWebSocket(self.junction, lane_average_speed, lane_queue_length, lane_total_car,self.ws)
            except:
                self.WebSocketOpen = False
        if SendToApi:
            SendDataToApi(self.junction, lane_average_speed, lane_queue_length, lane_total_car, "localhost", 31539)
        if FLAGS.dont_show:
            return
        color_light_blue = (0,255,255)
        draw_lane(frame, lanes, color = color_light_blue)
           
        # if enable info flag then print details about each track
        if FLAGS.info:
            print("Tracker ID: {}, Class: {},  carBox Coords (xmin, ymin, xmax, ymax): {}".format(str(track.track_id), class_name, (int(carBox[0]), int(carBox[1]), int(carBox[2]), int(carBox[3]))))

        # calculate frames per second of running detections
        
        result = np.asarray(frame)
        result = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        #cv2.imshow(f"Junction {self.junction}",result)
        self.result = maintain_aspect_ratio_resize(result, FLAGS.width)

if __name__ == '__main__':
    try:
        app.run(main)
    except SystemExit:
        pass
