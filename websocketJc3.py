import datetime
import ujson as json

class Lane:  
    JunctionName:str  
    Speed:float
    NumOfcar:float  
    LaneNumber:float  
    QueueLength: float  
    ServiceProvider: str

class Junction:
    JunctionName:str
    Lanes:list

def SendWebSocket( junctionName, lane_average_speed, lane_queue_length, lane_total_car, WebSocketConnection):   
    
    junction = Junction()
    junction.JunctionName = junctionName
    junction.Lanes = []
    jsonString = "["
    for i in range(len(lane_average_speed)):
        lane = Lane()
        lane.JunctionName = junctionName
        lane.LaneNumber = i
        lane.NumOfcar = int(lane_total_car[i])
        lane.Speed = lane_average_speed[i]
        lane.QueueLength = lane_queue_length[i]
        junction.Lanes.append(lane)
        jsonString = jsonString + json.dumps(lane.__dict__, indent=2)  
    jsonString = jsonString + "]"
    WebSocketConnection.send(jsonString)

