import cv2 as cv

cap = cv.VideoCapture('data/video/903_day.mp4')
cap1 = cv.VideoCapture('data/video/904_day.mp4')

if cap.isOpened()== False:
    print("Error camera 1 isn't connecting")
if cap1.isOpened()== False:
    print("Error camera 2 isn't connecting")

while (cap.isOpened() or cap1.isOpened()):
   ret, img = cap.read()
   ret1, img1 = cap1.read()
   if ret == True:
       cv.imshow('Video 1',img)
       cv.imshow('Video 2',img1)

   if cv.waitKey(20) and 0xFF == ord('q'):
        break

cap.release()
cap1.release()
cv.destroyAllWindows()
