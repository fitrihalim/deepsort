################################################
#Author : Huynh Son Bach                       #
#Company: LingKaiL                             #
#postgreSQL                                    # 
#This script using for checking database       #
#History of edit                               #
#1. MNFAH : 27 04 21 Change connection string  #                           
#2. MNFAH : 12 06 21 Modify updaterow function #                             
################################################

import psycopg2
from psycopg2 import sql
from config import database_config
from datetime import datetime
from query import *


import pandas as pd
from paramiko import SSHClient
from sshtunnel import SSHTunnelForwarder
from os.path import expanduser
import os

def real_time_current():
    now = datetime.now()
    return str(now)


def creat_database(database_name):
    params = database_config()
    params['database'] = 'postgres'
    con = psycopg2.connect(**params)
    con.autocommit = True
    cur = con.cursor()
    # sql.SQL and sql.Identifier are needed to avoid SQL injection attacks.
    cur.execute(sql.SQL('CREATE DATABASE {};').format(
        sql.Identifier(database_name)))


def createTable():
    """ Connect to the PostgreSQL database server """
    conn = None
    try:
        # read connection parameters
        params = database_config()

        # connect to the PostgreSQL server
        # print('Connecting to the PostgreSQL database...')
        connection = psycopg2.connect(**params)
        # create a cursor
        cur = connection.cursor()
        
        #define new table
        create_table_query = '''CREATE TABLE trafficqueuebylane
          (camera_id           TEXT,
          time         TEXT,
          lane TEXT PRIMARY KEY,
          qlength         TEXT,
          speed         TEXT,
          numofcar         TEXT); '''
	    # execute a statement
        cur.execute(create_table_query)
        connection.commit()
        # print("Table created successfully in PostgreSQL ")
       
	    # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()
            # print('Database connection closed.')


def insert_new_row(table_name, camera_id, time, lane, qlength, speed, noc):

    """ Connect to the PostgreSQL database server """
    connection = None
    try:
        # read connection parameters
        params = database_config()


        # connect to the PostgreSQL server
        # print('Connecting to the PostgreSQL database...')
        connection = psycopg2.connect(**params)

        # create a cursor
        cur = connection.cursor()
        
        # Insert statements

        sqlInsertRow  = "INSERT INTO " + table_name + " values('" + camera_id + "', '" + time + "', '" + lane + "', '" + qlength + "', '" + speed + "', '" + noc + "')"
        cur.execute(sqlInsertRow)
        connection.commit()
        # print("Updated data to table:", table_name)
       
	    # close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()
            # print('Database connection closed.')


def check_database():
    """ Connect to the PostgreSQL database server """
    connection = None
    try:
        # read connection parameters
        params = database_config()

        # connect to the PostgreSQL server
        # print('Connecting to the PostgreSQL database...')
        connection = psycopg2.connect(**params)

        # create a cursor
        cur = connection.cursor()
        
        # execute a statement
        # print('PostgreSQL database version:')
        cur.execute('SELECT version()')

        # display the PostgreSQL database server version
        db_version = cur.fetchone()
        # print(db_version)
        cur.execute("select * from information_schema.tables where table_name=%s", ('trafficqueuebylane',))
        if(bool(cur.rowcount)):
            pass
        else:
            createTable()
            insert_new_row("trafficqueuebylane", "10.20.0.18", real_time_current(), "903_1", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.0.18", real_time_current(), "903_2", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.0.18", real_time_current(), "903_3", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.0.18", real_time_current(), "903_4", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.1.95", real_time_current(), "904_1", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.1.95", real_time_current(), "904_2", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.1.95", real_time_current(), "904_3", "0", "0", "0")
            insert_new_row("trafficqueuebylane", "10.20.1.95", real_time_current(), "904_4", "0", "0", "0")
	# close the communication with the PostgreSQL
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        creat_database("trafficInfo")
        check_database()
    finally:
        if connection is not None:
            connection.close()
            # print('Database connection closed.')


def update_row(table_name, lane, speed, qlength, numberofcar, Connectionpool):

    query = f"UPDATE {table_name} SET speed = {speed}, qlength = {qlength}, numofcar= {numberofcar} , time= '{datetime.now()}'  WHERE lane = '{lane}'"
    #query = f"Select * from {table_name}"
    connection = Connectionpool.getconn()
    cur = connection.cursor()
    cur.execute(query)
    connection.commit()
    #cur.close()
    #connection.close()
    Connectionpool.putconn(connection)
    pass



    #try:
    #    query = f"UPDATE {table_name} SET speed = {speed}, qlength = {qlength}, numofcar= {numberofcar} WHERE lane = '{lane}'"
    #    postgresConnection.executeQuery(query)
    #except (Exception, psycopg2.DatabaseError) as error:
    #    print(error)
