#####################################################################################################################
# List of reference                                                                                                 #
#https://pynative.com/psycopg2-python-postgresql-connection-pooling/                                                #
#https://stackoverflow.com/questions/34965824/connecting-to-postgresql-database-through-ssh-tunneling-in-python?rq=1#
#https://towardsdatascience.com/how-to-query-postgresql-using-python-with-ssh-in-3-steps-cde626444817               #
#Modify and maintain by : Mohamad Nur Fitri bin Abd Halim - PM OF MALAYSIA                                          #
#####################################################################################################################

from sshtunnel import SSHTunnelForwarder
from config import database_config
import psycopg2
from psycopg2 import pool
from datetime import datetime

def CreatePool(config, ssh=True):
        # SSH Tunnel Variables
        pgres_host = config['pgres_host']
        pgsql_user = config['pgsql_user']
        pgsql_pass = config['pgsql_pass']
        pgres_port = config['pgres_port']
        db = config['db']
        ssh_user = config['ssh_user']
        ssh_host = config['ssh_host']
        ssh_port = config['ssh_port']
        ssh_password = config['ssh_password']
        
        if ssh == True:
            server = SSHTunnelForwarder(
                (ssh_host, int(ssh_port)),
                ssh_username=ssh_user,
                remote_bind_address=(pgres_host, int(pgres_port)),
                ssh_password = ssh_password,
            )
            server.start() #start ssh server
            local_port = server.local_bind_port
            print(f'Server connected via SSH || Local Port: {local_port}...')
            try:
                print("Success to create connection pool")
                return  psycopg2.pool.ThreadedConnectionPool(1, 100, database=db, user=pgsql_user, password=pgsql_pass, host='127.0.0.1', port=local_port)
            except Exception as e:
                print("Failed to create connection pool")

def Update_database( junction, lane_average_speed, lane_queue_length, lane_total_car, Connectionpool):
   
    query = ""
    for i in range(len(lane_average_speed)):
        query = query + f" UPDATE \"SkyNet\" SET speed = {lane_average_speed[i]}, qlength = {lane_queue_length[i]}, numofcar= {lane_total_car[i]} , time= '{datetime.now()}'  WHERE junction_name = '{junction}' and lane = '{str(i+1)}' ;"

    if(query != ""):
        connection = Connectionpool.getconn()
        cur = connection.cursor()
        cur.execute(query)
        connection.commit()
        cur.close()
        Connectionpool.putconn(connection)

def Insert_lane_if_not_exist(junction_name, num_of_lanes, Connectionpool):

    query = ""
    for i in range(num_of_lanes):
        query = query + f"INSERT INTO \"SkyNet\" (junction_name, lane) VALUES ({junction_name},{i+1}) ON CONFLICT (junction_name, lane) DO NOTHING; ";
    if(query != ""):
        connection = Connectionpool.getconn()
        cur = connection.cursor()
        cur.execute(query)
        connection.commit()
        cur.close()
        Connectionpool.putconn(connection)
