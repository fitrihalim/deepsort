import cv2
from config import lane_config
from checkinbox import getLaneConfig , draw_lane, clear , maintain_aspect_ratio_resize
from absl import app, flags
from absl.flags import FLAGS
flags.DEFINE_string('junction', '906', 'Junction code')

new_lanes,new_lines=[],[]
mouse_is_down = False

def on_mouse(event, x, y, flags, params):
    global mouse_is_down 
    if mouse_is_down and event == cv2.EVENT_LBUTTONUP:
        if len(new_lines) != 1:
            print('|', end='')
        print(f'{x}, {y}', end='')
        mouse_is_down = False
   
    elif event == cv2.EVENT_LBUTTONDOWN:
        if len(new_lines) == 0:
            new_lines.append([x,y])
        else:
            print('|', end='')
        print(f'{x}, {y}', end='')
        mouse_is_down = True
        new_lines.append([x,y])

    elif mouse_is_down and event == cv2.EVENT_MOUSEMOVE:
        new_lines[len(new_lines)-1]=[x,y]

def main(_argv):
    global new_lanes
    global new_lines
    video_path = getLaneConfig(lane_config(FLAGS.junction))[2]
    vid = cv2.VideoCapture(video_path)
    count = 0
    lanes = getLaneConfig(lane_config(FLAGS.junction))[0]
    new_lanes.append(new_lines)
    print('Press enter or space to create new lane, press esc to quit')
    print('Line1=',end='')
    while True:
        _, img = vid.read()
        width = 1000
        # resize image
        img = maintain_aspect_ratio_resize(img, width)

        cv2.namedWindow('real image')
        cv2.setMouseCallback('real image', on_mouse, 0)
        thickness = 1
        color_yellow = (0,255,255)
        color_blue = (255,0,0)

        draw_lane(img, lanes, 2, color=color_yellow)
        draw_lane(img, new_lanes, 2, color=color_blue)
        cv2.imshow('real image', img)
        #count += 1

        if count < 50:
            k = cv2.waitKey(33)
            if k == 27:
                cv2.destroyAllWindows()
                break
            elif k == 13 or k == ord(' '): #if button enter or space bar is inserted, create a new line
                print(f'\nLine{len(new_lanes)+1}=',end='')
                line_to_be_added = new_lines.copy()
                new_lines.clear()
                new_lanes[len(new_lanes)-1] = line_to_be_added
                new_lanes.append(new_lines)
            elif k == ord('c'):
                clear()
                print()
                new_lines=[]
                new_lanes=[]
                new_lanes.append(new_lines)
                print('Line1=',end='')
        elif count >= 50:
            if cv2.waitKey(0) == 27:
               cv2.destroyAllWindows()
               break
            count = 0

if __name__ == "__main__":
    app.run(main)

